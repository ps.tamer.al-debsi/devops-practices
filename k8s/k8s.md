# Kubernetes Notes 

**How To Install Minikube On Ubuntu 18.04**

Step 1: Update System and Install Required Packages

```
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install curl
sudo apt-get install apt-transport-https

```

Step 2: Install VirtualBox Hypervisor
```
sudo apt install virtualbox virtualbox-ext-pack

```

Step 3: Install Minikube

1. First, download the latest Minikube binary using the wget command:
```
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
```

2. Copy the downloaded file and store it into the /usr/local/bin/minikube directory with:
```
sudo cp minikube-linux-amd64 /usr/local/bin/minikube
```

3. Next, give the file executive permission using the chmod command:
```
sudo chmod 755 /usr/local/bin/minikube
```

4. Finally, verify you have successfully installed Minikube by checking the version of the software:
```
minikube version
```


Step 4: Install Kubectl
To deploy and manage clusters, you need to install kubectl, the official command line tool for Kubernetes.

1. Download kubectl with the following command:
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
```


2. Make the binary executable by typing:
```
chmod +x ./kubectl
```
3. Then, move the binary into your path with the command:
```
sudo mv ./kubectl /usr/local/bin/kubectl
```
4. Verify the installation by checking the version of your kubectl instance:
```
kubectl version -o json
```
Step 5: Start Minikube
Once you have set up all the required software, you are ready to start Minikube.

Run the following command:
```
minikube start
```
To see the kubectl configuration use the command:
```
kubectl config view
```

To show the cluster information:
```
kubectl cluster-info
```

To check running nodes use the following command:
```
kubectl get nodes
```
To see a list of all the Minikube pods run:
```
kubectl get pod
```

To ssh into the Minikube VM:
```
minikube ssh
```
To stop running the single node cluster type:
```
minikube stop
```
Command to stop Minikube and the expected terminal output.
To check its status use:
```
minikube status
```

To delete the single node cluster:
```
minikube delete
```
To see a list of installed Minikube add-ons:
```
minikube addons list
```

**Removing minikube**
```
On Linux
minikube stop; minikube delete
docker stop (docker ps -aq)
rm -r  ~/.minikube
sudo rm /usr/local/bin/localkube /usr/local/bin/minikube
systemctl stop '*kubelet*.mount'
sudo rm -rf /etc/kubernetes/
docker system prune -af --volumes
```
**KUBECTL COMMANDS**
 ## Common Commands

| Name                                | Command                                                                |
| ----------------------------------- | ---------------------------------------------------------------------- |
| List everything                     | `kubectl get all --all-namespaces`                                     |
| List pods with nodes info           | `kubectl get pod -o wide`                                              |
| Open a bash terminal in a pod       | `kubectl exec -it storage sh`                                          |
| kubectl run shell command           | `kubectl exec -it mytest -- ls -l /etc/hosts`                          |
| Check pod environment variables     | kubectl exec redis-master-ft9ex env                                  |
| Get system conf via configmap       | kubectl -n kube-system get cm kubeadm-config -o yaml                 |
| Explain resource                    | kubectl explain pods, kubectl explain svc                          |
| Get all services                    | kubectl get service --all-namespaces                                 |
| Get services sorted by name         | kubectl get services --sort-by.metadata.name                          |
| Get pods sorted by restart count    | kubectl get pods --sort-by'.status.containerStatuses[0].restartCount' |
| Validate yaml file with dry run     | kubectl create --dry-run --validate -f pod-dummy.yaml                |
| Enable kubectl shell autocompletion | echo "source <(kubectl completion bash)" >>~/.bashrc, and reload     |

## Check Performance

| Name                                         | Command                                              |
|--------------------------------------------- |----------------------------------------------------- |
| Get node resource usage                      | kubectl top node                                   |
| Get pod resource usage                       | `kubectl top pod`                                    |
| List resource utilization for all containers | kubectl top pod --all-namespaces                     |

## Resources Deletion

| Name                                    | Command                                                  |
|---------------------------------------- | -------------------------------------------------------- |
| Delete pod                              | `kubectl delete pod/<pod-name> -n <my-namespace>`        |
| Delete pods by labels                   | kubectl delete pod -l envtest                         |
| Delete deployments by labels            | `kubectl delete deployment -l appwordpress`             |
| Delete all resources filtered by labels | `kubectl delete pods,services -l namemyLabel`           |
| Delete resources under a namespace      | kubectl -n my-ns delete po,svc --all                   |
| Delete persist volumes by labels        | kubectl delete pvc -l appwordpress                    |
| Delete statefulset only (not pods)      | kubectl delete sts/<stateful_set_name> --cascade false |

## Pod

| Name                         | Command                                                                                                   |
|----------------------------- | --------------------------------------------------------------------------------------------------------- |
| List all pods                | `kubectl get pods`                                                                                        |
| List pods for all namespace  | `kubectl get pods -all-namespaces`                                                                        |
| List all critical pods       | kubectl get -n kube-system pods -a                                                                      |
| List pods with more info     | kubectl get pod -o wide, kubectl get pod/<pod-name> -o yaml                                           |
| Get pod info                 | `kubectl describe pod/srv-mysql-server`                                                                   |
| List all pods with labels    | kubectl get pods --show-labels                                                                          |
| Get Pod initContainer status | kubectl get pod --template '{{.status.initContainerStatuses}}' <pod-name>                               |
| kubectl run command          | kubectl exec -it -n "$ns" "$podname" -- sh -c "echo $msg >>/dev/err.log"                                  |

## Pod Advanced

| Name                             | Command                                                                                                                       |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| Get pod by selector              | podname$(kubectl get pods -n $namespace --selector"appsyslog" -o jsonpath'{.items[*].metadata.name}')                     |
| List pods and containers         | kubectl get pods -o'custom-columnsPODS:.metadata.name,CONTAINERS:.spec.containers[*].name'                                  |
| List pods, containers and images | kubectl get pods -o'custom-columnsPODS:.metadata.name,CONTAINERS:.spec.containers[*].name,Images:.spec.containers[*].image' |

## Label & Annontation

| Name                             | Command                                                               |
|--------------------------------- | --------------------------------------------------------------------- |
| Filter pods by label             | kubectl get pods -l ownerdenny                                     |
| Manually add label to a pod      | kubectl label pods dummy-input ownerdenny                          |
| Remove label                     | kubectl label pods dummy-input owner-                               |

## Deployment & Scale

| Name                         | Command                                                                  |
|----------------------------- | ------------------------------------------------------------------------ |
| Scale out                    | kubectl scale --replicas3 deployment/nginx-app                        |
| online rolling upgrade       | kubectl rollout app-v1 app-v2 --imageimg:v2                           |
| Roll backup                  | kubectl rollout app-v1 app-v2 --rollback                               |
| List rollout                 | kubectl get rs                                                         |
| Check update status          | kubectl rollout status deployment/nginx-app                            |
| Check update history         | kubectl rollout history deployment/nginx-app                           |
| Pause/Resume                 | kubectl rollout pause deployment/nginx-deployment, resume            |
| Rollback to previous version | kubectl rollout undo deployment/nginx-deployment                       |

## Service

| Name                            | Command                                                                           |
|-------------------------------- | --------------------------------------------------------------------------------- |
| List all services               | kubectl get services                                                            |
| List service endpoints          | kubectl get endpoints                                                           |
| Get service detail              | kubectl get service nginx-service -o yaml                                       |
| Get service cluster ip          | kubectl get service nginx-service -o go-template'{{.spec.clusterIP}}'            |
| Get service cluster port        | kubectl get service nginx-service -o go-template'{{(index .spec.ports 0).port}}' |
| Expose deployment as lb service | kubectl expose deployment/my-app --typeLoadBalancer --namemy-service          |
| Expose service as lb service    | kubectl expose service/wordpress-1-svc --typeLoadBalancer --namewordpress-lb  |

## StatefulSet

| Name                               | Command                                                  |
|----------------------------------- | -------------------------------------------------------- |
| List statefulset                   | kubectl get sts                                        |
| Delete statefulset only (not pods) | kubectl delete sts/<stateful_set_name> --cascade false |
| Scale statefulset                  | kubectl scale sts/<stateful_set_name> --replicas 5     |

## Volumes & Volume Claims

| Name                      | Command                                            |
|-------------------------- | -------------------------------------------------- |
| Check the mounted volumes | kubectl exec storage ls /data                    |
| Check persist volume      | kubectl describe pv/pv0001                       |
| List storage class        | kubectl get storageclass                         |
| Copy files                | kubectl cp /tmp/foo <namespace1>/<pod1>:/tmp/bar |

## Events & Metrics

| Name            | Command                               |
|---------------- | ------------------------------------- |
| View all events | kubectl get events --all-namespaces |

## Namespace & Security

| Name                         | Command                                        |
|----------------------------- | ---------------------------------------------- |
| List authenticated contexts  | kubectl config get-contexts                  |
| Switch context               | kubectl config use-context <cluster-name>    |
| Delete the specified context | kubectl config delete-context <cluster-name> |
| List all namespaces defined  | `kubectl get namespaces`                       |
| kubectl config file          | ~/.kube/config                               |

## Network

| Name                              | Command                                                  |
|---------------------------------- | -------------------------------------------------------- |
| Temporarily add a port-forwarding | kubectl port-forward redis-izl09 6379                  |
| Add port-forwaring for deployment | `kubectl port-forward deployment/redis-master 6379:6379` |
| Add port-forwaring for replicaset | kubectl port-forward rs/redis-master 6379:6379         |
| Add port-forwaring for service    | kubectl port-forward svc/redis-master 6379:6379        |
| Get network policy                | kubectl get NetworkPolicy                              |

## Check status

| Name                               | Summary                                    |
|----------------------------------- | ------------------------------------------ |
| List everything                    | `kubectl get all --all-namespaces`         |
| Get cluster info                   | kubectl cluster-info                     |
| Get configuration                  | kubectl config view                      |
| Get kubectl version                | kubectl version                          |
| Get component status               | kubectl get componentstatus              |
| Similar to docker ps             | `kubectl get nodes`                        |
| Similar to docker inspect        | `kubectl describe pod/nginx-app-413181-cn` |
| Similar to docker logs           | `kubectl logs`                             |
| Similar to docker exec           | `kubectl exec`                             |
| Get services for current namespace | kubectl get svc                          |
| Get node status                    | kubectl describe node/<node_name>        |

 k port-forward svc/devopsapp 809

||Action  ||                   Command                                                                                        URL 

forward service           kubectl  port-forward svc/devop-sapp 8090                                http://localhost:8090/actuator/health
forward pod direct      k port-forward devops-app-649554574d-trbcp 8090                  http://localhost:8090/actuator/health
                                      k port-forward devops-app-649554574d-trbcp 8090:8070        http://localhost:8070/actuator/health
without forward                                                                                           http://10.152.183.187:8090/actuator/health
# command toencod the username and pasword 
```
echo -n 'root' | base64
cm9vdA==
```