# Google Cloud notes

**Accessing our cluster**
'https://35.192.96.20/ '

```
Username :admin
Password : z42SjPcHh7G6SdJBZO
```
Cluster CA certificate

```
MIIDDDCCAfSgAwIBAgIRAPxe5XFtN7A1po0cDl9SCkQwDQYJKoZIhvcNAQELBQAw
LzEtMCsGA1UEAxMkNDljZWQ1OTktNDkxMi00NjY2LWEzOWQtZjQzZDZiOThlZDk1
MB4XDTIwMDYwMTA3MDM1NVoXDTI1MDUzMTA4MDM1NVowLzEtMCsGA1UEAxMkNDlj
ZWQ1OTktNDkxMi00NjY2LWEzOWQtZjQzZDZiOThlZDk1MIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEAt9frxOdihygctNrE4Lj60HSQal8xFe+K9J717p4a
Ggyt+oVQK1BWF/dYrKg1geK33/sN+9wVlgpAmd2szV6gSv4uOjnsOO1R+Ag4lg4f
2rVTRBXcWnjIHL3/IIjy3oHrL2oaxi0/Ci20CjAYboWXj3k8h14La8tEY3sOZW/S
cqbK5EOL+C10oSwlysNB1nKeVTClU1wrAw28qz2JYBNgFD3iUov38VGsWiGivi3v
wpZPSesBJm1vBWglaYBQlWzxqTaeib+Mfx6xmoeZb8Oq97qWaRC9FHkB4oRe6lCq
bbmQlQWRHiSAvsP18VJVCshWYzg9FlJCFolFXN+Kxf0uwQIDAQABoyMwITAOBgNV
HQ8BAf8EBAMCAgQwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEA
q7FLt9hzFgwzLKwDcD1ULZyHiLYmo/RjiKTrDTaeM6uVKOEcAGwTEIxwDTnTd5wf
f+V4js+ar32GpcbJy5U9W8Oem29Km8Hv/LohbCNnFGwP9NLw+ZP7MciaHlxqL91R
LKsZHuGJvWHwU0wO+U7juEvTuLn4rhBGvc4KAnmUQCufUNrjxueHbVXpQHnFeFoA
QKr9r1uSQxsa4lnZa/c2h2G9v5q0I2OIKoTiXNWlEXHfONEnAwsHsL9SpmSxKfuI
tvwoMzzos+Iw3qpA8VzjqWbBZ7+tKvMkaVdxbKmOpH7THVOYegnjoZ4LOzASimfX
x4LEWIgZkgiWg0feMHks3A==
```

# Installing with apt-get (Debian and Ubuntu only)

Cloud SDK is available in package format for installation on Debian and Ubuntu systems. This package contains the gcloud, gcloud alpha, gcloud beta, gsutil, and bq commands only. It does not include kubectl or the App Engine extensions required to deploy an application using gcloud commands. 

Note: If you are using an instance on Google Compute Engine, Cloud SDK is installed by default. You can still manually install Cloud SDK using the instructions below.

1. Add the Cloud SDK distribution URI as a package source:
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/a

# Add the Cloud SDK distribution URI as a package source
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Import the Google Cloud Platform public key
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

# Update the package list and install the Cloud SDK
sudo apt-get update && sudo apt-get install google-cloud-sdk