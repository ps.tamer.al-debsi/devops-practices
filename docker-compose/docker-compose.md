# Docker Compose Notes 

**Make sure your applicatyion is up on Compose by hit http://localhost:8090/actuator/health **

**Docker coposne commands**

1. terminal into the docker container
```
docker exec -it :container_id bash
```
2. run the docker container in debug mode
```
docker-compose run --service-ports web
```
3. build the docker container
```
docker-compose build
```
4. start the docker container
```
docker-compose up
```
5. start the docker container in the background
```
docker-compose up -d
```
6. list all container 
```
docker ps
docker-compose ps
```
7. remove all docker containers in the repository
```
docker-compose down
```
8. remove a specific docker container
```
docker kill :container_id
```
9. view all of the docker images
```
docker images
```

10. clean out any images, builds, etc., that might be hanging
```
docker system prune
```