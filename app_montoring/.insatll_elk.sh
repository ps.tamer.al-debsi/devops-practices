
echo "-----Add the Elastic Helm charts repo"

 helm repo add elastic https://helm.elastic.co

echo "--------Install 7.8.1 release: "

helm install --name elasticsearch --version 7.8.1 elastic/elasticsearch

echo " ------list helm chart"

helm list stable/elastic-stack
echo "--------add helm chart to your current dirctory" 

helm fetch --untar stable/elastic-stack 

echo " fix failed to create resource: Deployment.apps elastic-stack-elasticsearch-client is invalid: spec.template.spec.initContainers[0].securityContext.privileged: Forbidden: disallowed by cluster policy "


echo " Adding --allow-privileged into /var/snap/microk8s/current/args/kube-apiserver and microk8s.stop; microk8s.start resolves it. Thank you @ktsakalozos "

cd /home/tamer/space/devops/project/devops-practices/app_montoring 

helm upgrade \
--install -f  elastic-stack/values.yaml \
elastic-stack \
elastic-stack \
--force \
--debug \
--namespace devops


echo " * Within your cluster, at the following DNS name at port 9200:

    elastic-stack.devops.svc.cluster.local

  * From outside the cluster, run these commands in the same shell:

    export POD_NAME=$(kubectl get pods --namespace devops -l "app=elastic-stack,release=elastic-stack" -o jsonpath="{.items[0].metadata.name}")
    echo "Visit http://127.0.0.1:5601 to use Kibana"
       kubectl port-forward elastic-stack-kibana-774766bf47-f667t 5601 --namespace devops
    kubectl port-forward elastic-stack-kibana-774766bf47-f667t 5601 --namespace devops "

    kubectl scale deploy devopsapp-devopsapp --replicas=2 -n devops