echo "==========================what is auditbeat ?"
echo "As a DaemonSet to ensure that there’s a running instance on each node of the cluster. These instances are used to retrieve most metrics from the host, such as system metrics, Docker stats, and metrics from all the services running on top of Kubernetes.
As a single Metricbeat instance created using a Deployment. This instance is used to retrieve metrics that are unique for the whole cluster, such as Kubernetes events or kube-state-metrics."



kubectl create -f imetricbeat-kubernetes.yaml

sleep 5

kubectl --namespace=kube-system get ds/metricbeat


echo "===========================================delete metricbeat"

kubectl delete -f metricbeat-kubernetes.yaml

