#  k8s prometheus
helm repo add bitnami https://charts.bitnami.com/bitnami

helm fetch --untar bitnami/prometheus-operator


helm upgrade \
--install -f  prometheus-operator/values.yaml \
prometheus-operator \
prometheus-operator \
--force \
--debug \
--namespace devops


helm install stable/prometheus-operator --name prometheus-operator --namespace monitoring
kubectl port-forward -n monitoring prometheus-prometheus-operator-prometheus-0 9090


#  k8s grafana
kubectl port-forward $(kubectl get  pods --selector=app=grafana -n  monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring  3000


open your browser at http://localhost:3000, login as admin with password prom-operator

# Alertmanager
kubectl port-forward -n monitoring alertmanager-prometheus-operator-alertmanager-0 9093

http://localhost:9093 in your