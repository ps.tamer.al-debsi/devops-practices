
echo "--------add helm chart to your current dirctory" 

helm fetch --untar bitnami/prometheus-operator
echo "--------install" 
cd /home/tamer/space/devops/project/devops-practices/app_montoring 

helm upgrade \
--install -f  prometheus-operator/values.yaml \
prometheus-operator \
prometheus-operator \
--force \
--debug \
--namespace devops

echo ""
kubectl port-forward -n monitoring prometheus-prometheus-operator-prometheus-0 9090 -n devops
l
kubectl port-forward $(kubectl get  pods --selector=app=grafana -n  monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring  3000

echo "NOTES:** Please be patient while the chart is being deployed **

Watch the Prometheus Operator Deployment status using the command:

    kubectl get deploy -w --namespace devops -l app.kubernetes.io/name=prometheus-operator-operator,app.kubernetes.io/instance=prometheus-operator

Watch the Prometheus StatefulSet status using the command:

    kubectl get sts -w --namespace devops -l app.kubernetes.io/name=prometheus-operator-prometheus,app.kubernetes.io/instance=prometheus-operator

Prometheus can be accessed via port "9090" on the following DNS name from within your cluster:

    prometheus-operator-prometheus.devops.svc.cluster.local

To access Prometheus from outside the cluster execute the following commands:

    echo "Prometheus URL: http://127.0.0.1:9090/"
    kubectl port-forward --namespace devops svc/prometheus-operator-prometheus 9090:9090

Watch the Alertmanager StatefulSet status using the command:

    kubectl get sts -w --namespace devops -l app.kubernetes.io/name=prometheus-operator-alertmanager,app.kubernetes.io/instance=prometheus-operator

Alertmanager can be accessed via port "9093" on the following DNS name from within your cluster:

    prometheus-operator-alertmanager.devops.svc.cluster.local

To access Alertmanager from outside the cluster execute the following commands:

    echo "Alertmanager URL: http://127.0.0.1:9093/"
    kubectl port-forward --namespace devops svc/prometheus-operator-alertmanager 9093:9093
"