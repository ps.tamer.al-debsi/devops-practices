echo "==========================what is filebeat ?"
echo "You deploy Filebeat as a DaemonSet to ensure there’s a running instance on each node 
of the cluster.

The Docker logs host folder (/var/lib/docker/containers) is mounted on the Filebeat container.
 Filebeat starts an input for the files and begins harvesting them as soon as they appear in the
  folder.Everything is deployed under the kube-system namespace by default.
   To change the namespace, modify the manifest file."



kubectl create -f filebeat-kubernetes.yaml

sleep 5

kubectl --namespace=kube-system get ds/filebeat


echo "===========================================delete auditbeat"

kubectl delete -f filebeat-kubernetes.yaml

