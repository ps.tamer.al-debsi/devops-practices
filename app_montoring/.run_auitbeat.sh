echo "==========================what is auditbeat ?"
echo "
Auditbeat is able to monitor the file integrity of files in pods, to do that, 
the directories with the container root file systems have to be mounted as volumes in
 the Auditbeat container. For example, containers executed with containerd have their root
  file systems under /run/containerd. The reference manifest contains an example of this"



kubectl create -f auditbeat-kubernetes.yaml

sleep 5

kubectl --namespace=kube-system get ds/auditbeat


echo "===========================================delete auditbeat"

kubectl delete -f auditbeat-kubernetes.yaml

