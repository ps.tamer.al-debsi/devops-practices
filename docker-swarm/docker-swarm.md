# Docker Swarm Notes

**What is a swarm?**

The cluster management and orchestration features embedded in the Docker Engine are built using swarmkit. Swarmkit is a separate project which implements Docker’s orchestration layer and is used directly within Docker.

A swarm consists of multiple Docker hosts which run in swarm mode and act as managers (to manage membership and delegation) and workers (which run swarm services). A given Docker host can be a manager, a worker, or perform both roles. When you create a service, you define its optimal state (number of replicas, network and storage resources available to it, ports the service exposes to the outside world, and more). Docker works to maintain that desired state. For instance, if a worker node becomes unavailable, Docker schedules that node’s tasks on other nodes. A task is a running container which is part of a swarm service and managed by a swarm manager, as opposed to a standalone container.

**Docker Compose vs Docker Stack?**

Docker Compose is an official tool that helps you manage your Docker containers by letting you define everything through a docker-compose.yml file.

docker stack is a command that’s embedded into the Docker CLI. It lets you manage a cluster of Docker containers through Docker Swarm.

It just so happens both Docker Compose and the docker stack command support the same docker-compose.yml file with slightly different features.

**Docker Swarm Commands**
If you use Docker Machine, you can 

```
$ docker-machine ssh manager1
```
Run the following command to create a new swarm:
```
$ docker swarm init --advertise-addr <MANAGER-IP>
```

the following command creates a swarm on the manager1 machine:

```
 docker swarm init --advertise-addr 192.168.99.100
Swarm initialized: current node (dxn1zf6l61qsb1josjja83ngz) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.99.100:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Run the docker node ls command to view information about nodes:
```
$ docker node ls
```


Run the command produced by the docker swarm init output from the Create a swarm tutorial step to create a worker node joined to the existing swarm:

```
$ docker swarm join \
  --token  SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
  192.168.99.100:2377

This node joined a swarm as a worker.
```

If you don’t have the command available, you can run the following command on a manager node to retrieve the join command for a worker:

$ docker swarm join-token worker
```
To add a worker to this swarm, run the following command:

    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.99.100:2377
```

**Deploy a service to the swarm**

```
$ docker service create --replicas 1 --name helloworld alpine ping docker.com

9uk4639qpg7npwf3fn2aasksr
```
* The docker service create command creates the service.
* The --name flag names the service helloworld.
* The --replicas flag specifies the desired state of 1 running instance.
* The arguments alpine ping docker.com define the service as an Alpine Linux container that  executes the command ping docker.com.

Run docker service ls to see the list of running services:
```
$ docker service ls
```

**Deploy a stack to a swarm**

When running Docker Engine in swarm mode, you can use docker stack deploy to deploy a complete application stack to the swarm. The deploy command accepts a stack description in the form of a Compose file.
The docker stack deploy command supports any Compose file of version “3.0” or above

**REFERNCES**

* `
https://docs.docker.com/engine/swarm/key-concepts/
`
* `
https://docs.docker.com/engine/swarm/how-swarm-mode-works/services/
`
* `https://docs.docker.com/engine/reference/commandline/stack_deploy/
`

**run Swarm**
```
docker swarm init
```
**deploy stack from compose file**
```
docker stack deploy -c ./docker-compose.yml MYAPP
```
**list docker stack**
```
docker stack ls
```
**List services stack**
```
docker service ls
```
**remove services /stack**
````
docker stack rm $(docker stack ls |awk '{print $1}')
docker service  rm $(docker service ls -q)
````
