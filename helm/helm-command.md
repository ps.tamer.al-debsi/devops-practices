# Commands for working with repositories
``` 
helm repo list
```

NAME      URL
stable    https://kubernetes-charts.storage.googleapis.com/
``` 
helm search jenkins
```

NAME                VERSION       DESCRIPTION
stable/jenkins      0.1.14        A Jenkins Helm chart for Kubernetes.
```
 helm repo add my-charts https://my-charts.storage.googleapis.com 
 ```

``` 
helm repo list
```

NAME           URL
stable        https://kubernetes-charts.storage.googleapis.com/
my-charts     https://my-charts.storage.googleapis.com



kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'



First, the Helm CLI uses the Kubernetes CLI's configuration to connect to your current cluster.
```
~/.kube/config

$ kubectl config view
```
After it connects to your cluster, you use Helm installation commands to specify the attributes of the release.

To specify a release's name, use the --name flag:
```
$ helm install --name CustomerDB stable/mysql
$ helm install --name CustomerDB stable/mysql --version v1

```
To deploy the release into a Kubernetes namespace, use the --namespace flag:
```
$ helm install --namespace ordering-system stable/mysql
```
To override a value, use the --set flag:
```
$ helm install --set user.name='student',user.password='passw0rd' stable/mysql
```
To override values with a values file, use the --values or the --f flag:
```
$ helm install --values myvalues.yaml stable/mysql
```


Packaging charts
A chart is a directory. A Helm client can use chart directories on the same computer, but it's difficult to share with other users on other computers.

You package a chart by bundling the chart.yaml and related files into a .tar file and then installing the chart into a chart file:
```

    $ helm package <chart-path>

    $ helm install <chart-name>.tgz
```


To add a chart to a repository, copy it to the directory and regenerate the index:
```

    $ helm repo index <charts-path>  # Generates index of the charts in the repo
```

**helm install stable/mysql**
**NOTES**:

MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
brown-bunny-mysql.default.svc.cluster.local

To get your root password run:
```
    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace default brown-bunny-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)
```
To connect to your database:

1. Run an Ubuntu pod that you can use as a client:
```
    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il
```
2. Install the mysql client:
```
    $ apt-get update && apt-get install mysql-client -y
```
3. Connect using the mysql cli, then provide your password:
  ```
    $ mysql -h brown-bunny-mysql -p
```
To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
   ```
    kubectl port-forward svc/brown-bunny-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}
```

# Helm command reference

Install Tiller:
```
$ helm init
Create a chart:

$ helm create <chart>
List the repositories:

$ helm repo list
Search for a chart:

$ helm search <keyword>
Get information about a chart:

$ helm inspect <chart>
Deploy a chart (creates a release):

$ helm install <chart>
List all releases:

$ helm list --all
Get the status of a release:

$ helm status <release>
Get the details about a release:

$ helm get <release>
Upgrade a release:

$ helm upgrade <release> <chart>
Roll back a release:

$ helm rollback <release> <revision>
Delete a release:

$ helm delete <release>

-install helm to local repo
$ Helm fetch stable/wordpress
$ helm fetch --untar stable/wordpress

-check depedetsy and solve it 
 $ helm dep ls
 $ hel dep update

 -add index

$ helm repo index .

-add name tp repoo
$ helm repo add realmc(anyname) url-from-githin 
```
helm repo add stable/mysql stable/mysql:5.7.30

**helm command examples**

```
helm upgrade  --install \
               --atomic \
               --force \
               --wait \
               --timeout 900    \
               --namespace=kryptonite \
               --tiller-namespace=kryptonite \
               -f override-85237.yml checks-sib-ecc-master stack
			   
helm upgrade  --install   --namespace=kryptonite   --tiller-namespace=kryptonite  checks-sib-ecc-master stack	-f 	override-85237.yml 	   
			   
			   
			   
			    helm upgrade \
--install -f charts/ecc/values.yaml \
eccsite  \
--force \
--tiller-namespace kryptonite \
--namespace qc-test



helm upgrade \
--install -f stack/values.yaml \
eccsite  \
stack \
--force \                      
--tiller-namespace kryptonite \
--namespace qc-test \

➜  ~ helm upgrade  --install --force         --namespace=kryptonite \
               --tiller-namespace=kryptonite \
               -f override-85237.yml checks-sib-ecc-master stack
			   
			   
			   
 helm upgrade \                                                  
--install -f values.yaml \        
oab-demo-cicd5-pipeline  \                                                                                 
stack \
--force \                      
--tiller-namespace milkyway \
--namespace milkyway


helm upgrade --install  -f kryptonite-corpay/oab-demo.yaml --namespace=milkyway  --force --tiller-namespace=milkyway  oab-demo-cicd5-pipeline  kryptonite-corpay

helm delete --purge qc-mock  --tiller-namespace=kryptonite
kgp |grep system-config-24
helm delete --purge system-config-24-add-helm-char --tiller-namespace=kryptonite

system-config-24-add-helm-char
```
```
helm upgrade  --install -f devopsapp/values.yaml devopsapp devopsapp --force --tiller-namespace kube-system --namespace devops
```