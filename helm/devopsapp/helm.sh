export KUBECONFIG=~/.kube/config_local
kubectl create namespace devops                                

kubectl create secret docker-registry docker-hub-secret \
    -n devops \
    --docker-server=docker.io \
    --docker-username=tamerdibsidocker\
    --docker-password=P@ss123321 \
    -o yaml --dry-run | kubectl replace -n devops --force -f -


helm upgrade \
--install -f devopsapp/values.yaml \
test \
devopsapp \
--force \
--tiller-namespace kube-system \
--namespace devops 
